const events = () => {
    window.addEventListener('load', () => {
        console.log('Page loaded!');

        const joinSection = document.getElementById('join')
        if (joinSection !== null) join.style.display = 'block';

        const formDetails = document.getElementById('formSubscribe');
        const inputEmail = document.getElementById('inputSubscribe');
        if (inputEmail !== null) {
            formDetails.addEventListener('submit', (e) => {
                e.preventDefault();
                console.log('Email: ' + inputEmail.value);
                inputEmail.value = '';
            }, false)
        }
    });
}

class SectionCreator {
    create(type) {
        if (type === 'standard') return new Creator(type, 'Join Our Program', 'Subscribe');
        if (type === 'advanced') return new Creator(type, 'Join Our Advanced Program', 'Subscribe to Advanced');
    }
}

class Creator {
    constructor(type, title, button) {
        this.type = type;
        this.title = title;
        this.button = button;
    }
}

const obj = new SectionCreator()
const getTitle = document.getElementById('joinTitle');
const getButton = document.getElementById('joinButton');

function changeContentStandard() {
    window.addEventListener('load', () => {
        getTitle.innerHTML = obj.create('standard').title;
        getButton.innerHTML = obj.create('standard').button;
    });
};

function changeContentAdvance() {
    window.addEventListener('load', () => {
        getTitle.innerHTML = obj.create('advanced').title;
        getButton.innerHTML = obj.create('advanced').button;
    })
};

const remove = () => {
    document.getElementById('join').remove();
}

export { events, changeContentStandard, changeContentAdvance, remove };